#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <assert.h>

#define chemin "annuaire5000.csv"

struct PERSONNE
{
    int id;
    char nom [50];
    char prenom [50];
    char ville [50];
    char codePostal [50];
    char numero [50];
    char mail [50];
    char profession [50];
};

void menu();

struct PERSONNE * lecture_annuaire();

void affichage_annuaire();

void recherche(struct PERSONNE* annuaire);

void triParNom(struct PERSONNE *annuaire);

void triParPrenom(struct PERSONNE *annuaire);

void triParCodePostal(struct PERSONNE *annuaire);

void triParProfession(struct PERSONNE *annuaire);

void triParID(struct PERSONNE* annuaire);

void filtreNom(char *nom, struct PERSONNE *annuaire);

void filtrePrenom(const char *prenom, struct PERSONNE *annuaire);

void filtreCodePostal(const char *codePostal, struct PERSONNE *annuaire);

void filtreProfession(const char *profession, struct PERSONNE *annuaire);

void ajoute_client(struct PERSONNE *annuaire);

void modifie_client(struct PERSONNE *annuaire);

void supprime_client(struct PERSONNE * annuaire);

void sauvegarder(struct PERSONNE * annuaire);

char * strsep (char **stringp, const char *delim);

int nLignes = 0;

int nChampsVides;


int main() {

    struct PERSONNE* annuaire = lecture_annuaire();

    menu(annuaire);

    return 1;

}

void menu(struct PERSONNE *annuaire)
{

    int run = 1;

    int entree;

    while(run) {
        printf("MENU PRINCIPAL : \n"
               "1. Afficher l'annuaire.\n"
               "2. Trier par ...\n"
               "3. Filtrer par ...\n"
               "4. Rechercher \n"
               "5. Ajouter une personne.\n"
               "6. Modifier une personne.\n"
               "7. Supprimer une personne.\n"
               "8. Sauvegarder.\n"
               "9. Quitter.\n");

        scanf("%d", &entree);
        fflush(stdin);

        switch (entree) {
            case (1): {
                affichage_annuaire(annuaire);
                break;
            }
            case (2): {
                int typeTri;

                printf("1. Tri par nom.\n"
                       "2. Tri par prenom.\n"
                       "3. Tri par code postal.\n"
                       "4. Tri par profession.\n"
                       "5. Tri par ID.\n"
                       "6. Quitter\n");

                scanf("%d", &typeTri);

                switch (typeTri) {
                    case (1): {
                        triParNom(annuaire);
                        break;
                    }

                    case (2): {
                        triParPrenom(annuaire);
                        break;
                    }

                    case (3): {
                        triParCodePostal(annuaire);
                        break;
                    }

                    case (4): {
                        triParProfession(annuaire);
                        break;
                    }

                    case (5): {
                        triParID(annuaire);
                        break;
                    }
                    case(6):
                    {
                        break;
                    }

                    default: {
                        printf("Erreur, entree invalide.\n");
                        break;
                    }
                }
                break;
            }
            case (3): {

                int typeFiltre;
                char filtre[50];

                printf("1. Filtrer par nom.\n"
                       "2. Filtrer par prenom.\n"
                       "3. Filtrer par code postal.\n"
                       "4. Filtrer par profession.\n"
                       "5. Quitter\n");

                scanf("%d",&typeFiltre);
                fflush(stdin);

                printf("Filtre : ");

                scanf("%s", (char*)&filtre);
                fflush(stdin);;

                printf("\n");

                switch(typeFiltre)
                {
                    case (1):
                    {
                        filtreNom(filtre, annuaire);
                        break;
                    }
                    case (2) : {
                        filtrePrenom(filtre, annuaire);
                        break;
                    }
                    case (3) :
                    {
                        filtreCodePostal(filtre, annuaire);
                        break;
                    }
                    case (4) :
                    {
                        filtreProfession(filtre, annuaire);
                        break;
                    }
                    case (5):
                    {
                        break;
                    }
                    default:
                    {
                        printf("Erreur, entree invalide.\n");
                    }
                }

                break;
            }
            case (4): {
                recherche(annuaire);

                break;
            }
            case (5):{
                ajoute_client(annuaire);
                break;
            }
            case (6): {
                modifie_client(annuaire);
                break;
            }
            case (7): {
                supprime_client(annuaire);
                break;
            }
            case (8):
            {
                sauvegarder(annuaire);
                break;
            }
            case (9):
            {
                run = 0;
                break;
            }
            default: {
                printf("Erreur : entree invalide");

                break;
            }
        }
    }
}

void affichage_annuaire(struct PERSONNE * annuaire)
{
    printf("Que voulez vous afficher ?\n"
           "1. Annuaire complet.\n"
           "2. Lignes comportant des champs vides uniquement.\n"
           "3. Une personne en particulier\n"
           "4. Lignes ne comportant pas de champ vide dans le champ...\n");

    int entree;
    scanf("%d",&entree);
    fflush(stdin);

    switch (entree) {
        case (1):
        {
            for (int i =0;i<nLignes+1;i++)
            {
                printf("Id : %d\n",annuaire[i].id);
                printf("Prenom : %s \n",annuaire[i].prenom);
                printf("Nom : %s \n",annuaire[i].nom);
                printf("Ville : %s \n",annuaire[i].ville);
                printf("Code postal : %s \n",annuaire[i].codePostal);
                printf("mail : %s \n",annuaire[i].mail);
                printf("profession : %s \n",annuaire[i].profession);

                printf("-----------------------------------------------\n");
            }
            printf("Nlignes = %d, %d lignes comportent un ou plusieurs champ(s) vide(s).\n",nLignes, nChampsVides);

            break;
        }
        case(2):
        {
            for(int i =0; i < nLignes; i++)
            {
                if(strcmp(annuaire[i].prenom,"ChampVide")==0 || strcmp(annuaire[i].nom,"ChampVide")==0 || strcmp(annuaire[i].ville,"ChampVide")==0 ||
                        strcmp(annuaire[i].codePostal,"ChampVide")==0 || strcmp(annuaire[i].mail,"ChampVide")==0 || strcmp(annuaire[i].profession,"ChampVide")==0)
                {
                    printf("Id : %d\n",annuaire[i].id);
                    printf("Prenom : %s \n",annuaire[i].prenom);
                    printf("Nom : %s \n",annuaire[i].nom);
                    printf("Ville : %s \n",annuaire[i].ville);
                    printf("Code postal : %s \n",annuaire[i].codePostal);
                    printf("mail : %s \n",annuaire[i].mail);
                    printf("profession : %s \n",annuaire[i].profession);

                    printf("-----------------------------------------------\n");
                }
            }
            break;

        }
        case(3):
        {

            printf("Entrez l'ID de la personne a afficher, -1 si vous ne le connaissez pas : ");
            int ID;
            scanf("%d",&ID);
            fflush(stdin);
            printf("\n");

            if(ID==-1)
            {
                recherche(annuaire);
            }
            else
            {
                printf("Id : %d\n",annuaire[ID].id);
                printf("Prenom : %s \n",annuaire[ID].prenom);
                printf("Nom : %s \n",annuaire[ID].nom);
                printf("Ville : %s \n",annuaire[ID].ville);
                printf("Code postal : %s \n",annuaire[ID].codePostal);
                printf("mail : %s \n",annuaire[ID].mail);
                printf("profession : %s \n",annuaire[ID].profession);
            }

        }
        case(4):
        {
            printf("1. Lignes sans champ vide dans prenom.\n"
                   "2. Lignes sans champ vide dans nom.\n"
                   "3. Lignes sans champ vide dans ville.\n"
                   "4. Lignes sans champ vide dans code postal.\n"
                   "5. Lignes sans champ vide dans mail.\n"
                   "6. Lignes sans champ vide dans profession.\n"
                   "7. Lignes sans aucun champ vide.\n");

            scanf("%d",&entree);
            fflush(stdin);

            switch (entree) {
                case (1):
                {
                    for(int i = 0; i < nLignes; i++)
                    {
                        if(strcmp(annuaire[i].prenom, "ChampVide")!=0)
                        {
                            printf("Id : %d\n",annuaire[i].id);
                            printf("Prenom : %s \n",annuaire[i].prenom);
                            printf("Nom : %s \n",annuaire[i].nom);
                            printf("Ville : %s \n",annuaire[i].ville);
                            printf("Code postal : %s \n",annuaire[i].codePostal);
                            printf("mail : %s \n",annuaire[i].mail);
                            printf("profession : %s \n",annuaire[i].profession);

                            printf("-----------------------------------------------\n");
                        }
                    }
                }
                case (2):
                {
                    for(int i = 0; i < nLignes; i++)
                    {
                        if(strcmp(annuaire[i].nom, "ChampVide")!=0)
                        {
                            printf("Id : %d\n",annuaire[i].id);
                            printf("Prenom : %s \n",annuaire[i].prenom);
                            printf("Nom : %s \n",annuaire[i].nom);
                            printf("Ville : %s \n",annuaire[i].ville);
                            printf("Code postal : %s \n",annuaire[i].codePostal);
                            printf("mail : %s \n",annuaire[i].mail);
                            printf("profession : %s \n",annuaire[i].profession);

                            printf("-----------------------------------------------\n");
                        }
                    }
                }
                case (3):
                {
                    for(int i = 0; i < nLignes; i++)
                    {
                        if(strcmp(annuaire[i].ville, "ChampVide")!=0)
                        {
                            printf("Id : %d\n",annuaire[i].id);
                            printf("Prenom : %s \n",annuaire[i].prenom);
                            printf("Nom : %s \n",annuaire[i].nom);
                            printf("Ville : %s \n",annuaire[i].ville);
                            printf("Code postal : %s \n",annuaire[i].codePostal);
                            printf("mail : %s \n",annuaire[i].mail);
                            printf("profession : %s \n",annuaire[i].profession);

                            printf("-----------------------------------------------\n");
                        }
                    }
                }
                case (4):
                {
                    for(int i = 0; i < nLignes; i++)
                    {
                        if(strcmp(annuaire[i].codePostal, "ChampVide")!=0)
                        {
                            printf("Id : %d\n",annuaire[i].id);
                            printf("Prenom : %s \n",annuaire[i].prenom);
                            printf("Nom : %s \n",annuaire[i].nom);
                            printf("Ville : %s \n",annuaire[i].ville);
                            printf("Code postal : %s \n",annuaire[i].codePostal);
                            printf("mail : %s \n",annuaire[i].mail);
                            printf("profession : %s \n",annuaire[i].profession);

                            printf("-----------------------------------------------\n");
                        }
                    }
                }
                case (5):
                {
                    for(int i = 0; i < nLignes; i++)
                    {
                        if(strcmp(annuaire[i].mail, "ChampVide")!=0)
                        {
                            printf("Id : %d\n",annuaire[i].id);
                            printf("Prenom : %s \n",annuaire[i].prenom);
                            printf("Nom : %s \n",annuaire[i].nom);
                            printf("Ville : %s \n",annuaire[i].ville);
                            printf("Code postal : %s \n",annuaire[i].codePostal);
                            printf("mail : %s \n",annuaire[i].mail);
                            printf("profession : %s \n",annuaire[i].profession);

                            printf("-----------------------------------------------\n");
                        }
                    }
                }
                case (6):
                {
                    for(int i = 0; i < nLignes; i++)
                    {
                        if(strcmp(annuaire[i].profession, "ChampVide")!=0)
                        {
                            printf("Id : %d\n",annuaire[i].id);
                            printf("Prenom : %s \n",annuaire[i].prenom);
                            printf("Nom : %s \n",annuaire[i].nom);
                            printf("Ville : %s \n",annuaire[i].ville);
                            printf("Code postal : %s \n",annuaire[i].codePostal);
                            printf("mail : %s \n",annuaire[i].mail);
                            printf("profession : %s \n",annuaire[i].profession);

                            printf("-----------------------------------------------\n");
                        }
                    }
                }
                case (7):
                {
                    for(int i = 0; i < nLignes; i++)
                    {
                        if(strcmp(annuaire[i].prenom, "ChampVide")!=0 && strcmp(annuaire[i].nom, "ChampVide")!=0
                        && strcmp(annuaire[i].ville, "ChampVide")!=0 && strcmp(annuaire[i].codePostal, "ChampVide")!=0
                        && strcmp(annuaire[i].mail, "ChampVide")!=0 &&strcmp(annuaire[i].profession, "ChampVide")!=0 )
                        {
                            printf("Id : %d\n",annuaire[i].id);
                            printf("Prenom : %s \n",annuaire[i].prenom);
                            printf("Nom : %s \n",annuaire[i].nom);
                            printf("Ville : %s \n",annuaire[i].ville);
                            printf("Code postal : %s \n",annuaire[i].codePostal);
                            printf("mail : %s \n",annuaire[i].mail);
                            printf("profession : %s \n",annuaire[i].profession);

                            printf("-----------------------------------------------\n");
                        }
                    }
                }
            }

        }

    }


}

struct PERSONNE* lecture_annuaire(struct PERSONNE * annuaire)
{
    errno = 0;

    FILE *fichier = fopen(chemin,"r");

    int c = 0;

    char ligne[500];

    char *token = NULL;

    annuaire = (struct PERSONNE * ) malloc((5000)*sizeof(struct PERSONNE));
    assert(annuaire != NULL);


    if (fichier == NULL)
    {
        printf("Erreur : %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }

    while (fgets(ligne, 500, fichier) != NULL) {

        //printf("ligne : %s, i = %d\n",ligne, c);

        struct PERSONNE client;

        int index = 0;

        char *copie_ligne = strdup(ligne);

        nLignes++;

        bool champVide = false;

        while ((token = strsep(&copie_ligne, ",")) != NULL)
        {

            //printf("token = %s\n",token);
            if (*token == '\n') {

                // continuer
            }
            else if (*token == NULL)
            {
                switch (index)
                {
                    case (0): {
                        strcpy(client.prenom,"ChampVide");
                        if(champVide == false)
                        {
                            nChampsVides ++;
                            champVide = true;
                        }
                    }

                    case (1): {
                        strcpy(client.nom,"ChampVide");
                        if(champVide == false)
                        {
                            nChampsVides ++;
                            champVide = true;
                        }
                    }


                    case (2): {
                        strcpy(client.ville,"ChampVide");
                        if(champVide == false)
                        {
                            nChampsVides ++;
                            champVide = true;
                        }
                    }


                    case (3): {
                        strcpy(client.codePostal,"ChampVide");
                        if(champVide == false)
                        {
                            nChampsVides ++;
                            champVide = true;
                        }
                    }


                    case (4): {
                        strcpy(client.numero,"ChampVide");
                        if(champVide == false)
                        {
                            nChampsVides ++;
                            champVide = true;
                        }
                    }


                    case (5): {
                        strcpy(client.mail,"ChampVide");
                        if(champVide == false)
                        {
                            nChampsVides ++;
                            champVide = true;
                        }
                    }


                    case (6): {
                        strcpy(client.profession,"ChampVide");
                        if(champVide == false)
                        {
                            nChampsVides ++;
                            champVide = true;
                        }
                    }
                }
            }
            else
            {
                switch (index)
                {

                    case (0): {
                        if (islower(token[0]) > 0 )
                        {
                            token[0] = (char)toupper(token[0]);
                        }
                        strcpy(client.prenom,token);
                    }

                    case (1): {
                        if (islower(token[0]) > 0 )
                        {
                            token[0] = (char)toupper(token[0]);
                        }
                        strcpy(client.nom,token);
                    }

                    case (2): {
                        strcpy(client.ville,token);
                    }

                    case (3): {

                        strcpy(client.codePostal,token);
                    }

                    case (4): {
                        strcpy(client.numero,token);
                    }

                    case (5): {
                        strcpy(client.mail,token);
                        }

                    case (6): {
                        if(strcmp(token,client.mail) == 0)
                        {
                            strcpy(client.profession,"ChampVide");
                        }
                        else
                        {
                            if (isupper(token[0]) > 0)
                            {
                                token[0] = (char) tolower(token[0]);
                            }
                            strcpy(client.profession,token);;
                        }
                    }
                }
            }
            index++;
        }
        client.id = c;
        free(copie_ligne);
        annuaire[c] = client;
        c++;
    }
    nLignes--;
    fclose(fichier);
    return(annuaire);
}

void recherche(struct PERSONNE * annuaire)
{
    printf("Type de recherche :\n"
           "1. Par nom\n"
           "2. Par prenom\n"
           "3. Par numero de telephone\n"
           "4. Par mail\n"
           "5. Par id\n"
           "6. Quitter\n");
    int entree = (int) NULL;
    char recherche[50];

    scanf("%d",&entree);
    fflush(stdin);

    switch (entree) {
        case (1):
        {
            printf("Nom a rechercher :\n ");
            scanf("%s",&recherche);
            fflush(stdin);
            for(int i =0; i < nLignes; i++)
            {
                if (strcmp(annuaire[i].nom,recherche) == 0)
                {
                    printf("Id : %d\n",annuaire[i].id);
                    printf("Prenom : %s \n",annuaire[i].prenom);
                    printf("Nom : %s \n",annuaire[i].nom);
                    printf("Ville : %s \n",annuaire[i].ville);
                    printf("Code postal : %s \n",annuaire[i].codePostal);
                    printf("mail : %s \n",annuaire[i].mail);
                    printf("profession : %s \n",annuaire[i].profession);

                    printf("-----------------------------------------------\n");
                }
            }
            break;
        }
        case(2):
        {
            printf("Prenom a rechercher : ");
            scanf("%s",&recherche);
            fflush(stdin);
            printf("\n");

            for(int i = 0; i < nLignes; i++)
            {
                if(strcmp(annuaire[i].prenom, recherche)==0)
                {
                    printf("Id : %d\n",annuaire[i].id);
                    printf("Prenom : %s \n",annuaire[i].prenom);
                    printf("Nom : %s \n",annuaire[i].nom);
                    printf("Ville : %s \n",annuaire[i].ville);
                    printf("Code postal : %s \n",annuaire[i].codePostal);
                    printf("mail : %s \n",annuaire[i].mail);
                    printf("profession : %s \n",annuaire[i].profession);

                    printf("-----------------------------------------------\n");
                }
            }
            break;
        }
        case(3):
        {
            printf("Numero a rechercher :\n ");
            scanf("%s",&recherche);
            fflush(stdin);
            for(int i =0; i<nLignes;i++)
            {
                if(strcmp(annuaire[i].numero,recherche) == 0)
                {
                    printf("Id : %d\n",annuaire[i].id);
                    printf("Prenom : %s \n",annuaire[i].prenom);
                    printf("Nom : %s \n",annuaire[i].nom);
                    printf("Ville : %s \n",annuaire[i].ville);
                    printf("Code postal : %s \n",annuaire[i].codePostal);
                    printf("mail : %s \n",annuaire[i].mail);
                    printf("profession : %s \n",annuaire[i].profession);

                    printf("-----------------------------------------------\n");
                }
            }
            break;
        }
        case (4):
        {
            printf("Mail a rechercher :\n ");
            scanf("%s",(char *)recherche);
            fflush(stdin);
            for(int i=0; i < nLignes; i++)
            {
                if(strcmp(annuaire[i].mail, recherche)==0)
                {
                    printf("Id : %d\n",annuaire[i].id);
                    printf("Prenom : %s \n",annuaire[i].prenom);
                    printf("Nom : %s \n",annuaire[i].nom);
                    printf("Ville : %s \n",annuaire[i].ville);
                    printf("Code postal : %s \n",annuaire[i].codePostal);
                    printf("mail : %s \n",annuaire[i].mail);
                    printf("profession : %s \n",annuaire[i].profession);

                    printf("-----------------------------------------------\n");
                }
            }
            case (5):
            {
                int ID;
                printf("ID a rechercher :\n ");
                scanf("%d",&ID);
                fflush(stdin);
                printf("Id : %d\n",annuaire[ID].id);
                printf("Prenom : %s \n",annuaire[ID].prenom);
                printf("Nom : %s \n",annuaire[ID].nom);
                printf("Ville : %s \n",annuaire[ID].ville);
                printf("Code postal : %s \n",annuaire[ID].codePostal);
                printf("mail : %s \n",annuaire[ID].mail);
                printf("profession : %s \n",annuaire[ID].profession);

                printf("-----------------------------------------------\n");

            }
        }

    }



}

void triParNom(struct PERSONNE *annuaire)
{


    int fin_non_triee = nLignes;

    bool fin = false;

    struct PERSONNE temp;

    while (fin_non_triee > 0 && fin == false)
    {
        fin = true;

        for(int i = 0; i < fin_non_triee-1; i++)
        {
            if (strcoll(annuaire[i].nom,annuaire[i + 1].nom) > 0)
            {
                fin = false;
                temp = annuaire[i + 1];
                annuaire[i + 1] = annuaire[i];
                annuaire[i] = temp;
                //printf("oui\n");
            }
        }

        fin_non_triee --;

    }
}

void triParPrenom(struct PERSONNE *annuaire)
{

    int fin_non_triee = nLignes;

    bool fin = false;

    struct PERSONNE temp;

    while (fin_non_triee > 0 && fin == false)
    {
        fin = true;

        for(int i = 0; i < fin_non_triee-1; i++)
        {
            if (strcoll(annuaire[i].prenom,annuaire[i + 1].prenom) > 0)
            {
                fin = false;
                temp = annuaire[i + 1];
                annuaire[i + 1] = annuaire[i];
                annuaire[i] = temp;
                //printf("oui\n");
            }
        }

        fin_non_triee --;

    }
}

void triParCodePostal(struct PERSONNE *annuaire)
{

    int fin_non_triee = nLignes;

    bool fin = false;

    struct PERSONNE temp;

    while (fin_non_triee > 0 && fin == false)
    {
        fin = true;

        for(int i = 0; i < fin_non_triee-1; i++)
        {
            if (strcoll(annuaire[i].codePostal,annuaire[i + 1].codePostal) > 0)
            {
                fin = false;
                temp = annuaire[i + 1];
                annuaire[i + 1] = annuaire[i];
                annuaire[i] = temp;
                //printf("oui\n");
            }
        }

        fin_non_triee --;

    }
}

void triParProfession(struct PERSONNE *annuaire)
{


    int fin_non_triee = nLignes;

    bool fin = false;

    struct PERSONNE temp;

    while (fin_non_triee > 0 && fin == false)
    {
        fin = true;

        for(int i = 0; i < fin_non_triee-1; i++)
        {
            if (strcoll(annuaire[i].profession,annuaire[i + 1].profession) > 0)
            {
                fin = false;
                temp = annuaire[i + 1];
                annuaire[i + 1] = annuaire[i];
                annuaire[i] = temp;
                //printf("oui\n");
            }
        }

        fin_non_triee --;

    }
}

void triParID(struct PERSONNE *annuaire)
{


    int fin_non_triee = nLignes;

    bool fin = false;

    struct PERSONNE temp;

    while (fin_non_triee > 0 && fin == false)
    {
        fin = true;

        for(int i = 0; i < fin_non_triee-1; i++)
        {
            if (annuaire[i].id > annuaire[i+1].id)
            {
                fin = false;
                temp = annuaire[i + 1];
                annuaire[i + 1] = annuaire[i];
                annuaire[i] = temp;
                //printf("oui\n");
            }
        }

        fin_non_triee --;

    }
}

void filtreNom(char *nom, struct PERSONNE *annuaire)
{

    for(int i =0; i < nLignes; i++)
    {
        if(strstr(annuaire[i].nom,nom) != NULL)
        {
            printf("ID : %d\n",annuaire[i].id);
            printf("Prenom : %s \n", annuaire[i].prenom);
            printf("Nom : %s \n", annuaire[i].nom);
            printf("Ville : %s \n", annuaire[i].ville);
            printf("Code postal : %s \n", annuaire[i].codePostal);
            printf("mail : %s \n", annuaire[i].mail);
            printf("profession : %s \n", annuaire[i].profession);

            printf("-----------------------------------------------\n");
        }
    }
}

void filtrePrenom(const char *prenom, struct PERSONNE *annuaire)
{
    for(int i =0; i < nLignes; i++)
    {
        if(strstr(annuaire[i].prenom,prenom) != NULL)
        {
            printf("ID : %d\n",annuaire[i].id);
            printf("Prenom : %s \n", annuaire[i].prenom);
            printf("Nom : %s \n", annuaire[i].nom);
            printf("Ville : %s \n", annuaire[i].ville);
            printf("Code postal : %s \n", annuaire[i].codePostal);
            printf("mail : %s \n", annuaire[i].mail);
            printf("profession : %s \n", annuaire[i].profession);

            printf("-----------------------------------------------\n");
        }
    }
}

void filtreCodePostal(const char *codePostal, struct PERSONNE *annuaire)
{
    for(int i =0; i < nLignes; i++)
    {
        if(strstr(annuaire[i].codePostal,codePostal) != NULL)
        {
            printf("ID : %d\n",annuaire[i].id);
            printf("Prenom : %s \n", annuaire[i].prenom);
            printf("Nom : %s \n", annuaire[i].nom);
            printf("Ville : %s \n", annuaire[i].ville);
            printf("Code postal : %s \n", annuaire[i].codePostal);
            printf("mail : %s \n", annuaire[i].mail);
            printf("profession : %s \n", annuaire[i].profession);

            printf("-----------------------------------------------\n");
        }
    }
}

void filtreProfession(const char *profession, struct PERSONNE *annuaire)
{
    for(int i =0; i < nLignes; i++)
    {
        if(strstr(annuaire[i].profession,profession) != NULL)
        {
            printf("ID : %d\n",annuaire[i].id);
            printf("Prenom : %s \n", annuaire[i].prenom);
            printf("Nom : %s \n", annuaire[i].nom);
            printf("Ville : %s \n", annuaire[i].ville);
            printf("Code postal : %s \n", annuaire[i].codePostal);
            printf("mail : %s \n", annuaire[i].mail);
            printf("profession : %s \n", annuaire[i].profession);

            printf("-----------------------------------------------\n");
        }
    }
}

void ajoute_client(struct PERSONNE * annuaire)
{

    nLignes++;
    annuaire = ( struct PERSONNE * ) realloc( annuaire , (nLignes+1) * sizeof ( struct PERSONNE ) );
    assert(annuaire != NULL);

    char champ[50];
    assert(champ != NULL);

    annuaire[nLignes].id = nLignes;

    printf("Entrez le prenom : "); scanf("%s",(char *) &champ); fflush(stdin); printf("\n");
    strcpy(annuaire[nLignes].prenom, champ);

    printf("Entrez le nom : "); scanf("%s",(char *) &champ); fflush(stdin);printf("\n");
    strcpy(annuaire[nLignes].nom, champ);

    printf("Entrez la ville : "); scanf("%s",(char *) &champ); fflush(stdin);printf("\n");
    strcpy(annuaire[nLignes].ville, champ);

    printf("Entrez le code postal : "); scanf("%s",(char *)&champ); fflush(stdin);printf("\n");
    strcpy(annuaire[nLignes].codePostal, champ);

    printf("Entrez le mail : "); scanf("%s",(char *)&champ); fflush(stdin);printf("\n");
    strcpy(annuaire[nLignes].mail, champ);

    printf("Entrez la profession : "); scanf("%s",(char *)&champ); fflush(stdin);printf("\n");
    strcpy(annuaire[nLignes].profession, champ);

    printf("Nouvelle personne ajoutee a l'annuaire.\n");
}

void modifie_client(struct PERSONNE * annuaire)
{
    printf("Si vous connaissez l'Id de la personne a supprimer, entrez la. Sinon, entrez -1 : ");
    int ID;
    scanf("%d",&ID);
    fflush(stdin);
    printf("\n");

    struct PERSONNE nvClient;

    if(ID == -1)
    {
        recherche(annuaire);
    }
    else
    {

        printf("Entrez le nouveau prenom, -1 pour ne pas le modifier.");

        char champ[50];
        scanf("%s",(char*)&champ);
        fflush(stdin);

        printf("\n");

        if(strcmp(champ,"-1")!=0)
        {
            strcpy(nvClient.prenom,champ);
            printf("1\n");
        }
        else
        {
            strcpy(nvClient.prenom,annuaire[ID].prenom);
            printf("2\n");
        }


        printf("Entrez le nouveau nom, -1 pour ne pas le modifier.");

        scanf("%s",(char*)&champ);
        fflush(stdin);

        printf("\n");

        if(strcmp(champ,"-1")!=0)
        {
            strcpy(nvClient.nom,champ);
        }
        else
        {
            strcpy(nvClient.nom,annuaire[ID].nom);
        }


        printf("Entrez la nouvelle ville, -1 pour ne pas la modifier.");

        scanf("%s",(char*)&champ);
        fflush(stdin);

        printf("\n");

        if(strcmp(champ,"-1")!=0)
        {
            strcpy(nvClient.ville,champ);;
        }
        else
        {
            strcpy(nvClient.ville,annuaire[ID].ville);
        }


        printf("Entrez le nouveau code postal, -1 pour ne pas le modifier.");

        scanf("%s",(char*)&champ);
        fflush(stdin);

        printf("\n");

        if(strcmp(champ,"-1")!=0)
        {
            strcpy(nvClient.codePostal,champ);
        }
        else
        {
            strcpy(nvClient.codePostal,annuaire[ID].codePostal);
        }


        printf("Entrez le nouveau mail, -1 pour ne pas le modifier.");

        scanf("%s",(char*)&champ);
        fflush(stdin);

        printf("\n");

        if(strcmp(champ,"-1")!=0)
        {
            strcpy(nvClient.mail,champ);
        }
        else
        {
            strcpy(nvClient.mail,annuaire[ID].mail);
        }


        printf("Entrez la nouvelle profession, -1 pour ne pas la modifier.");

        scanf("%s",(char*)&champ);
        fflush(stdin);

        printf("\n");

        if(strcmp(champ,"-1")!=0)
        {
            strcpy(nvClient.profession,champ);
        }
        else
        {
            strcpy(nvClient.profession,annuaire[ID].profession);
        }


    }
    annuaire[ID].id = ID;
    strcpy(annuaire[ID].prenom,nvClient.prenom);
    strcpy(annuaire[ID].nom,nvClient.nom);
    strcpy(annuaire[ID].ville,nvClient.ville);
    strcpy(annuaire[ID].codePostal,nvClient.codePostal);
    strcpy(annuaire[ID].mail,nvClient.mail);
    strcpy(annuaire[ID].profession,nvClient.profession);
}

void supprime_client(struct PERSONNE * annuaire)
{
    printf("Si vous connaissez l'Id de la personne a supprimer, entrez la. Sinon, entrez -1 : ");
    int ID;
    scanf("%d",&ID);
    printf("\n");
    if (ID == -1)
    {
        recherche(annuaire);
    }
    else
    {
        for(int i = 0;i < nLignes; i++)
        {
            /*
            if(ID == nLignes)
            {
                realloc(annuaire, nLignes-1);
            }
             */
            if (annuaire[i].id == ID)
            {
                for (int j = i; j < nLignes-1; j++)
                {
                    annuaire[j] = annuaire[j+1];
                }
                break;
            }
        }
        printf("Personne supprimee avec succes.\n");
        nLignes --;
    }
}

void sauvegarder(struct PERSONNE * annuaire)
{

    errno = 0;
    FILE * fichier = fopen(chemin,"w");

    if(fichier == NULL)
    {
        printf("ERREUR : %s",strerror(errno));
    }

    for(int i =0; i < nLignes; i++)
    {
        if(strcmp(annuaire[i].prenom,"ChampVide")!=0)
        {
            fputs(annuaire[i].prenom,fichier);
        }
        fputc(',',fichier);

        if(strcmp(annuaire[i].nom,"ChampVide")!=0)
        {
            fputs(annuaire[i].nom,fichier);
        }
        fputc(',',fichier);

        if(strcmp(annuaire[i].ville,"ChampVide")!=0)
        {
            fputs(annuaire[i].ville,fichier);
        }
        fputc(',',fichier);

        if(strcmp(annuaire[i].codePostal,"ChampVide")!=0)
        {
            fputs(annuaire[i].codePostal,fichier);
        }
        fputc(',',fichier);

        if(strcmp(annuaire[i].numero,"ChampVide")!=0)
        {
            fputs(annuaire[i].numero,fichier);
        }
        fputc(',',fichier);

        if(strcmp(annuaire[i].profession,"ChampVide")!=0)
        {
            fputs(annuaire[i].profession,fichier);
        }

    }
}


char * strsep (char **stringp, const char *delim)

{
    char *begin, *end;
    begin = *stringp;
    if (begin == NULL)
        return NULL;
    // Find the end of the token.
    end = begin + strcspn (begin, delim);
    if (*end)
    {
        // Terminate the token and set *STRINGP past NUL character.
        *end++ = '\0';
        *stringp = end;
    }
    else
        // No more delimiters; this is the last token.
        *stringp = NULL;
    return begin;
}
